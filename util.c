//
// Created by vargat on 2019. 11. 10.
//
#include <stdio.h>
#include <stdlib.h>
#include "util.h"

void usage(char *appname) {
    printf("usage:%s <lb hb> lb: lower boundary(only decimal int) hb: upper boundary(only decimal int)\n", appname);
}

int intCheck(const char *strNumber) {
    if (!strNumber)
        return EXIT_FAILURE;

    int i = 0;
    if (*strNumber == '-' || *strNumber == '+')
        ++i;

    for (; *(strNumber + i) != '\0'; i++) {

        if (*(strNumber + i) < '0' || *(strNumber + i) > '9') {
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}

int intConvert(const char *strNumber) {
    int convertedNumber = atoi(strNumber);
    return convertedNumber;
}

int swapInt(int *firstNumber, int *secondNumber) {
    int temp;
    if (!firstNumber || !secondNumber) {
#ifdef DEBUG
        printf("Null pointer");
#endif
        return EXIT_FAILURE;
    } else {
        temp = *firstNumber;
        *firstNumber = *secondNumber;
        *secondNumber = temp;

        return EXIT_SUCCESS;
    }
}