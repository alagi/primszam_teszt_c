//
// Created by vargat on 2019. 11. 10..
//

#ifndef PRIMSZAM_TESZT_C_PRIME_H
#define PRIMSZAM_TESZT_C_PRIME_H

void printPrime(int, int);
int isPrime(int);
int* eratos(int);

#endif //PRIMSZAM_TESZT_C_PRIME_H
