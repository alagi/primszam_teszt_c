//
// Created by vargat on 2019. 11. 10.
//

#ifndef PRIMSZAM_TESZT_C_UTIL_H
#define PRIMSZAM_TESZT_C_UTIL_H

#define DEBUG

void usage(char *);
int intCheck(const char *);
int intConvert(const char *);
int swapInt(int*, int*);

#endif //PRIMSZAM_TESZT_C_UTIL_H