//
// Created by vargat on 2019. 11. 10..
//
#include <stdio.h>
#include <stdlib.h>
#include "prime.h"

void printPrime(int from, int to) {
    int i = from < 2 ? 2 : from;

    while (i < 6) {
        if (isPrime(i))
            printf("%d\n", i);
        i++;
    }

    for (; i <= to; i++) {

        if (i % 6 == 1 || i % 6 == 5) {
            if (isPrime(i))
                printf("%d\n", i);
        }
    }
}

int isPrime(int testNumber) {
    int j;
    if (testNumber % 2 == 0 && testNumber != 2)
        return 0;

    for (j = 3; j * j <= testNumber; j += 2) {
        if (testNumber % j == 0)
            return 0;
    }
    return 1;
}

int* eratos(int to) {

    int *numberArray = (int *) malloc(to * sizeof(int));

    int i, j;
    *(numberArray + 0) = 0;
    *(numberArray + 1) = 0;

    for (i = 2; i <= to; i++) {
        *(numberArray + i) = 1;
    }

    for (i = 2; i * i <= to; i++) {
        if (*(numberArray + i) == 1) {
            for (j = i * i; j <= to; j += i) {
                *(numberArray + j) = 0;
            }
        }
    }

    return numberArray;
}

