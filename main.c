//
// Created by vargat on 2019. 11. 10.
//
#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "prime.h"

int process(char **argv) {

    int *primeArray;
    int i;
    int lowerBoundary, upperBoundary;

    if (!(argv)) {
#ifdef DEBUG
        printf("Null pointer");
#endif
        return EXIT_FAILURE;
    }

    if (!intCheck(*(argv + 1)) && !intCheck(*(argv + 2))) {
        lowerBoundary = intConvert(*(argv + 1));
        upperBoundary = intConvert(*(argv + 2));
        if (upperBoundary < lowerBoundary) {
            swapInt(&lowerBoundary, &upperBoundary);
        }
#ifdef DEBUG
        printf("Converted lowerBoundary: %d\n", lowerBoundary);
        printf("Converted upperBoundary: %d\n", upperBoundary);
#endif // DEBUG
    } else {

#ifdef DEBUG
        printf("arg1: %s\n", *(argv + 1));
        printf("arg2: %s\n", *(argv + 2));
#endif // DEBUG
        usage(*argv);
        return EXIT_FAILURE;
    }

    //simple algorithm:

    //printPrime(lowerBoundary, upperBoundary);

    //2nd algorithm: Sieve of Eratosthenes:
    primeArray = eratos(upperBoundary);

    if (!primeArray) {
#ifdef DEBUG
        printf("Null pointer\n");
#endif
        return EXIT_FAILURE;
    }

    for (i = lowerBoundary<0?0:lowerBoundary; i <= upperBoundary; i++) {
        if (*(primeArray+i) == 1) {
            printf("%d\n", i);
        }
    }
    free(primeArray);
    return EXIT_SUCCESS;

}

int main(int argc, char **argv) {
    if (argc != 3) {
        printf("Invalid number of arguments!\n");
        usage(*argv);
        return EXIT_FAILURE;
    }
    return process(argv);
}